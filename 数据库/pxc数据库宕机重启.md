# mysql pxc模式部署三个节点都宕机后如何重启
## 背景
客户pxc模式部署的mysql都在同一个宿主机下创建的虚拟机中，宿主机被关闭了
## 环境信息
系统 centos7.4
cpu 8核
内存 32g
数据库为单独三台虚拟化服务器部署，这里分别命名为mysql-pxc01,mysql-pxc02,mysql-pxc03
## 操作步骤
1. 停掉所有机器pxc容器
```shell
$ sudo docker stop mysql-pxc01
$ sudo docker stop mysql-pxc02
$ sudo docker stop mysql-pxc03
```
2. 查看三台服务器的这个文件 /data/kube/mysql/grastate.dat，找到uuid尾数最大的服务器，将safe_to_bootstrap后面的值0 改成1，如果都一致则任意选一个服务器操作，我这里以mysql-pxc01为例
```shell
$ sudo cat /data/kube/mysql/grastate.dat
version: 2.1
uuid:    e104174a-3e08-11eb-9939-0e1af44b71bf
seqno:   -1
safe_to_bootstrap: 0
$ sudo vi /data/kube/mysql/grastate.dat ##将此处的safe_to_bootstrap：0改为1
$ sudo cat /data/kube/mysql/grastate.dat
version: 2.1
uuid:    e104174a-3e08-11eb-9939-0e1af44b71bf
seqno:   -1
safe_to_bootstrap: 1
```

3. 修改mysql本地配置文件，在/etc/mysql/conf.d/my.cnf 最后一行添加这一行，wsrep_cluster_address = gcomm://, **这里注意在全部数据库启动成功后，再去除掉新加的这一行，然后再次重启这里的数据库 **
```shell
$ cat /etc/mysql/conf.d/my.cnf
[mysqld]
log_queries_not_using_indexes = on
binlog_format = row
innodb_buffer_pool_size = 4G
enforce_gtid_consistency = on
server_id = 742
expire_logs_days = 7
slow_query_log = on
open_files_limit = 65536
log-bin = /var/lib/mysql/mysql-bin
long_query_time = 3
skip_name_resolve = true
log_output = file
gtid_mode = on_permissive
port = 3306
slow_query_log_file = /var/lib/mysql/mysql-slow.log
max_connections = 3000
wsrep_cluster_address = gcomm://
```
4. 重启这个服务器的mysql容器,启动成功后再依次启动mysql-pxc02,和mysql-pxc03
```shell
sudo docker start mysql-pxc01
sudo docker start mysql-pxc02
sudo docker start mysql-pxc03
```
5. 去除mysql-pxc01的my.cnf配置文件
```shell
$ cat /etc/mysql/conf.d/my.cnf
[mysqld]
log_queries_not_using_indexes = on
binlog_format = row
innodb_buffer_pool_size = 4G
enforce_gtid_consistency = on
server_id = 742
expire_logs_days = 7
slow_query_log = on
open_files_limit = 65536
log-bin = /var/lib/mysql/mysql-bin
long_query_time = 3
skip_name_resolve = true
log_output = file
gtid_mode = on_permissive
port = 3306
slow_query_log_file = /var/lib/mysql/mysql-slow.log
max_connections = 3000
```
6. 重启mysql-pxc01的mysql容器
`sudo docker start mysql-pxc01`

