# mysql pxc模式部署三个节点都宕机后如何重启
## 背景
客户pxc模式部署的mysql都在同一个宿主机下创建的虚拟机中，宿主机被关闭了
## 环境信息
pxc模式数据库3台
## 具体配置以及解释
```
server_id = 100
default-time-zone = '+8:00' #设置时区，这样mysql里面的慢日志等时间会和当前一致
log_timestamps = SYSTEM #log_timestamps 参数默认使用 UTC 时区，这样会使得日志中记录的时间比中国这边的慢了 8 个小时，导致查看日志不方便。修改为 SYSTEM 就能解决问题
character-set-server = utf8mb4  #utf8mb4编码是utf8编码的超集，兼容utf8，并且能存储4字节的表情字符。 采用utf8mb4编码的好处是：存储与获取数据的时候，不用再考虑表情字符的编码与解码问题，我们现有业务默认使用utf8mb4。
default_storage_engine = InnoDB #永久表（permanent tables）的默认存储引擎，云文档现有业务均采用innodb引擎。
open_files_limit = 65536 #打开的文件描述符限制
external-locking = FALSE #如果你有多台服务器使用同一个数据库目录（不建议），那么每台服务器都必须开启external locking；
lock_wait_timeout = 60  #事务锁超时时间
lower_case_table_names = 1  #MySQL不区分大小写,mysql8.0initialize之后无法修改
explicit_defaults_for_timestamp = 1 #动态修改时间戳，默认为1，后续会废弃该参数
log_bin_trust_function_creators = 1 #如果设置为0（默认值），用户不得创建或修改存储函数，除非它们具有除CREATE ROUTINE或ALTER ROUTINE特权之外的SUPER权限，如果变量设置为1，MySQL不会对创建存储函数实施这些限制，如果数据库没有使用主从复制，那么就可以将参数log_bin_trust_function_creators设置为1。
max_allowed_packet = 128M #用于设定所接受的包的大小，最大值是1G(1073741824)，如果设置超过1G，查看最终生效结果也只有1G。
max_connections = 2000 #如果服务器的并发连接请求量比较大，建议调高此值，以增加并行连接数量，当然这建立在机器能支撑的情况下，因为如果连接数越多，介于MySql会为每个连接提供连接缓冲区，就会开销越多的内存，所以要适当调整该值，不能盲目提高设值。可以过'conn%'通配符查看当前状态的连接数量，以定夺该值的大小。
wait_timeout = 28800 #服务器关闭非交互连接之前等待活动的秒数。
interactive_timeout = 28800 #服务器关闭交互式连接前等待活动的秒数。
binlog_cache_size = 4M #没有什么大事务，dml也不是很频繁的情况下可以设置小一点，如果事务大而且多，dml操作也频繁，则可以适当的调大一点，前者建议是1048576  --1M，后者建议是： 2097152 -- 4194304  即 2--4M
max_binlog_cache_size = 8G  #指的是binlog 能够使用的最大cache 内存大小
max_binlog_size = 1G #bin log日志每达到设定大小后，会使用新的bin log日志
binlog_format = row #row完整记录但是文件较大，STATEMENT文件小有可能缺失记录
binlog_checksum = 1 #binlog增加校验位
binlog_row_image = full #FULL记录每一行的变更，minimal只记录影响后的行。
log-bin = /var/lib/mysql/mysql-bin #binlog存放路径
expire_logs_days = 30 #binlog过期时间
binlog_rows_query_log_events = 1 #在row模式下..开启该参数,将把sql语句打印到binlog日志里面
binlog_error_action = ABORT_SERVER #MySQL在写 binlog 遇到严重错误时直接退出
transaction_write_set_extraction = xxhash64 # binlog_transaction_dependency_tracking 设置为WRITESET_SESSION后不可为空，可以为xxhash64或者MURMUR32
binlog_transaction_dependency_tracking = WRITESET_SESSION #设置事务在从库是顺序执行的，这就消除了我们上面说的从库查看数据可能会存在和主库不同的情况。设置为这个值会虽然会降低并行复制的性能，但是相比默认设置来说，性能还是有很大提升的。
relay_log_recovery = 1 #在数据库启动后立即启动自动relay log恢复，在恢复过程中，创建一个新的relay log文件，将sql线程的位置初始化到新的relay log，并将i/o线程初始化到sql线程位置。
relay-log-purge = 1 #为了让从库是 crash safe 的，必须设置 relay_log_recovery=1
gtid_mode = on #全局事物标识
enforce_gtid_consistency = 1 #当启用enforce_gtid_consistency功能的时候，MySQL只允许能够保障事务安全，并且能够被日志记录的SQL语句被执行
log_slave_updates = 1 #log-slave-updates参数默认时关闭的状态，如果不手动设置，那么bin-log只会记录直接在该库上执行的SQL语句，由replication机制的SQL线程读取relay-log而执行的SQL语句并不会记录到bin-log
skip_slave_start = 1  # 在slave服务器重启时，不会自动启动复制链路。默认情况下slave服务器重启后，mysql会自动启动复制链路，如果这个时候存在问题，则主从链路会中断，所以正常情况下，我们应该在服务器重启后检查是否存在问题，然后再手动启动复制链路
slave_net_timeout = 30 #slave_net_timeout表示slave在slave_net_timeout时间之内没有收到master的任何数据(包括binlog，heartbeat)，slave认为连接断开，会进行重连
master_info_repository = TABLE #如果参数是file，就会创建master.info文件，如果参数值是table，就在mysql中创建slave_master_info的表。
relay_log_info_repository = TABLE #如果relay_log_info_repository=file，就会创建一个realy-log.info，如果relay_log_info_repository=table，就会创建mysql.slave_relay_info表来记录同步的位置信息。
slave_load_tmpdir = /data/mysql #同步时候把load data之类的数据保存的目录。
slave_type_conversions = ALL_NON_LOSSY #如果从库类型比主库类型小，比如从int 复制到tinyint 这个参数就会起作用,ALL_LOSSY： 允许数据截断;ALL_NON_LOSSY： 不允许数据截断，如果从库类型大于主库类型，是可以复制的,反过来，就不行了，从库报复制错误，复制终止;ALL_LOSSY,ALL_NON_LOSSY: 所有允许的转换都会执行，而不管是不是数据丢失;空值 （不设置） ： 要求主从库的数据类型必须严格一致，否则都报错。
slave-parallel-type = LOGICAL_CLOCK #DATABASE: 默认值，基于库的并行复制方式,LOGICAL_CLOCK：基于组提交的并行复制方式
slave-parallel-workers = 16 #并行复制线程数
slave_pending_jobs_size_max = 256M #设置从库执行事件需要内存的大小，注意，需要大于主库max_allowed_packet的大小
slave_preserve_commit_order = 1 #当一个线程等待其他线程的事务提交时，会出现一个状态信息，在一个写入量较大的主从复制集群中，在从库上执行show processlist可以看到这个状态信息，如下：
slave_rows_search_algorithms = 'INDEX_SCAN,HASH_SCAN' #hash_scan提高性能总结：表中没有任何索引或者有索引且本条update/delete的关键字重复值较多
query_cache_size = 0 #查询缓存大小
query_cache_type = 0 #0时表示关闭，1时表示打开，2表示只要select 中明确指定SQL_CACHE才缓存。
innodb_buffer_pool_size = 1024M #主要针对InnoDB表性能影响最大的一个参数。功能与Key_buffer_size一样。InnoDB占用的内存，除innodb_buffer_pool_size用于存储页面缓存数据外，另外正常情况下还有大约8%的开销，主要用在每个缓存页帧的描述、adaptive hash等数据结构，如果不是安全关闭，启动时还要恢复的话，还要另开大约12%的内存用于恢复，两者相加就有差不多21%的开销。假设：12G的innodb_buffer_pool_size，最多的时候InnoDB就可能占用到14.5G的内存。若系统只有16G，而且只运行MySQL，且MySQL只用InnoDB,那么为MySQL开12G，是最大限度地利用内存了。
innodb_buffer_pool_instances = 8 #可以开启多个内存缓冲池，把需要缓冲的数据hash到不同的缓冲池中，这样可以并行的内存读写。实验环境下， innodb_buffer_pool_instances=8 在很小的 buffer_pool 大小时有很大的不同，而使用大的 buffer_pool 时，innodb_buffer_pool_instances=1 的表现最棒。
innodb_buffer_pool_load_at_startup = 1  #默认启用。指定在MySQL服务器启动时，InnoDB缓冲池通过加载之前保存的相同页面自动预热。
innodb_buffer_pool_dump_at_shutdown = 1 #默认启用。指定在MySQL服务器关闭时是否记录在InnoDB缓冲池中缓存的页面，以便在下次重新启动时缩短预热过程。
innodb_flush_neighbors = 0 #设置为0时，表示刷脏页时不刷其附近的脏页。设置为1时，表示刷脏页时连带其附近毗连的脏页一起刷掉。设置为2时，表示刷脏页时连带其附近区域的脏页一起刷掉。1与2的区别是2刷的区域更大一些。
innodb_flush_log_at_trx_commit = 1 #值为0 : 提交事务的时候，不立即把 redo log buffer 里的数据刷入磁盘文件的，而是依靠 InnoDB 的主线程每秒执行一次刷新到磁盘。此时可能你提交事务了，结果 mysql 宕机了，然后此时内存里的数据全部丢失。值为1 : 提交事务的时候，就必须把 redo log 从内存刷入到磁盘文件里去，只要事务提交成功，那么 redo log 就必然在磁盘里了。注意，因为操作系统的“延迟写”特性，此时的刷入只是写到了操作系统的缓冲区中，因此执行同步操作才能保证一定持久化到了硬盘中。值为2 : 提交事务的时候，把 redo 日志写入磁盘文件对应的 os cache 缓存里去，而不是直接进入磁盘文件，可能 1 秒后才会把 os cache 里的数据写入到磁盘文件里去。只有1才能真正地保证事务的持久性，但是由于刷新操作 fsync() 是阻塞的，直到完成后才返回，我们知道写磁盘的速度是很慢的，因此 MySQL 的性能会明显地下降。如果不在乎事务丢失，0和2能获得更高的性能。
innodb_log_buffer_size = 64M #该参数确保有足够大的日志缓冲区来保存脏数据在被写入到日志文件。
innodb_log_file_size = 1G #该参数决定着mysql事务日志文件（ib_logfile0）的大小；一般来说，日志文件的全部大小，应该足够容纳服务器一个小时的活动内容。
innodb_log_files_in_group = 3 #该参数控制日志文件数。默认值为2。mysql 事务日志文件是循环覆写的。如果想把原来是2的修改成3，这样的话你需要先关闭mysql服务，把原来的ib_logfile0和ib_logfile1文件删掉，然后启动mysql
innodb_undo_log_truncate = 1 #即开启在线回收（收缩）undo log日志文件，支持动态设置
innodb_undo_directory = /var/lib/mysql/mysql_data
innodb_undo_tablespaces = 4 #参数必须大于或等于2，即回收（收缩）一个undo log日志文件时，要保证另一个undo log是可用的。
innodb_max_undo_log_size = 4G #当超过这个阀值（默认是1G），会触发truncate回收（收缩）动作，truncate后空间缩小到10M。
innodb_io_capacity = 4000 # 好的方法是测量你的存储设置的随机写吞吐量，然后给innodb_io_capacity_max设置为你的设备能达到的最大IOPS。innodb_io_capacity就设置为它的50-75%,测试命令sudo fio -name iops -allow_mounted_write=1 -rw=randwrite -bs=4k -runtime=60 -iodepth 32 -filename /dev/sda1 -ioengine libaio -direct=1
innodb_io_capacity_max = 8000
innodb_write_io_threads = 8  #利用多核处理器 
innodb_read_io_threads = 8 #利用多核处理器 
innodb_purge_threads = 4 #负责回收已经使用并分配的undo页线程数
innodb_page_cleaners = 4 #执行脏页刷新操作线程数
innodb_open_files = 65536 #这个选项仅与你使用多表空间时有关。它指定InnoDB一次可以保持打开的.ibd文件的最大数目
innodb_flush_method = O_DIRECT #fdatasync模式：写数据时，write这一步并不需要真正写到磁盘才算完成（可能写入到操作系统buffer中就会返回完成），真正完成是flush操作，buffer交给操作系统去flush,并且文件的元数据信息也都需要更新到磁盘；O_DSYNC模式：写日志操作是在write这步完成，而数据文件的写入是在flush这步通过fsync完成；O_DIRECT模式：数据文件的写入操作是直接从mysql innodb buffer到磁盘的，并不用通过操作系统的缓冲，而真正的完成也是在flush这步,日志还是要经过OS缓冲
innodb_lru_scan_depth = 4000 #page cleaner线程每次刷脏页的数量
innodb_checksum_algorithm = crc32 #计算checksum的方式，crc32可以使用cpu硬件加速
innodb_lock_wait_timeout = 60 #设置锁等待的时间
innodb_rollback_on_timeout = 1 #设置为1超时后整个事务回滚
innodb_print_all_deadlocks = 1 #保存死锁日志
innodb_file_per_table = 1 #innodb可以把每个表的数据单独保存
innodb_online_alter_log_max_size = 4G #控制在用于在Online DDL操作时的一个临时的日志文件的上限值大小。如果该日志超过了innodb_online_alter_log_max_size的最大上限，DDL操作则会抛出失败，并且回滚所有未提交的DML操作。反过来说，该值如果设置更高，则可以允许在做Online DDL时，有更多的DML操作发生。但因此带来的问题就是，在DDL做完之后，需要更多时间来锁表和应用这些日志
internal_tmp_disk_storage_engine = InnoDB #定义磁盘临时表的引擎类型为 InnoDB
innodb_status_file = 1 #开启后会在datadir下生成一个innodb_status.pid文件, 周期性15秒向这个文件输出show engine innodb status. 如果异常关闭数据库, 这个文件不会被删除
innodb_status_output = 0 #这两个参数建议关闭（0）（我司配置关闭），否则会把数据库监控的信息全部记录到err log中，使错误日志增长过快，造成磁盘空间的使用紧张。
innodb_status_output_locks = 0
innodb_stats_on_metadata = 0  #设置为1，每当查询information_schema元数据库里的表时，Innodb还会随机提取其他数据库每个表索引页的部分数据，所以设置为0
```