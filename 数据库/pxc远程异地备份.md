# mysql pxc模式部署异地备份
## 背景
## 环境信息 
系统 centos7.4
cpu 8核
内存 32g
数据库为单独三台虚拟化服务器部署，这里分别命名为mysql-pxc01,mysql-pxc02,mysql-pxc03
## 操作步骤（由于我们的镜像中已经打包了备份工具，所以可以直接操作）
1. 现在mysql-pxc01上进入mysql容器
`$ docker exec -it mysql-pxc01 bash`
2. 全量备份到/tmp/full目录下
`$ innobackupex   --user=$db_user --password=$db_pwd  --no-timestamp $db_full`
3. 增量备份
`$ innobackupex  --user=$db_user  --password=$db_pwd   --no-timestamp --incremental  --incremental-basedir=$db_full  $db_incremental`
4. 这些我内容都放到脚本里面了，可以根据自己看看脚本以及按照自己的需求修改，脚本在附件中，名字《mysql_data_back3.sh》
5. 定时任务配置(这里docker命令写docker命令的绝对路径，具体以自己本地的为准,由于mysql内容映射到了本地/data/kube/mysql/目录下，所以我们只要rsync同步该目录下back文件夹下所有内容即可，这里要保证本机root用户到远程机器ssh可以免密登录)
```shell
$ sudo crontab -l
0 0 * *  6  /data/kube/bin/docker exec  mysql-pxc01 bash /var/lib/mysql/mysql_data_back.sh Full
0 1 * * 0-5  /data/kube/bin/docker exec  mysql-pxc01 bash /var/lib/mysql/mysql_data_back.sh incremental
0 3 * * * rsync --rsh=ssh   -rz /data/kube/mysql/back/ wps@ 目标机器ip:/backdir/
```
6. 恢复准备(由于宿主机没有安装xtrabackup工具，所以这些操作都是需要进入到mysql容器内部执行的，或者本地安装一下xtrabackup工具)
6.1 先应用全备日志（--apply-log，暂时不需要做回滚操作--redo-only）
```shell
$ innobackupex --apply-log --redo-only /backup/xfull/
```
6.2 合并增量到全备中（一致性的合并，如果有多次增量备份内容的话，需要多次执行下面命令，按周日增量合并到周六全量备份，然后周一增量合并到周六全量备份这样的顺序，保证增量的开始lsn等于全量的结束lsn）
```shell
$ innobackupex --apply-log --incremental-dir=/backup/xinc1 /backup/xfull/
```
6.3 恢复数据前的准备(合并xtabackup_log_file和备份的物理文件)，查看合并后的 checkpoints 其中的类型变为 full-prepared 即为可恢复。
```shell
$ innobackupex --apply-log /backup/xfull
$ cat /backup/xfull/xtrabackup_checkpoints 
backup_type = full-prepared
from_lsn = 0
to_lsn = 4114824
last_lsn = 4114824
compact = 0
recover_binlog_info = 0
```
7. 恢复方法(说明：无论使用那种恢复方法都要恢复后需改属组属主，保持与程序一致。)
7.1 恢复到单机
a. 方法一:直接将备份文件复制回来
```shell
$ cp -a /backup/full/ /var/lib/mysql/
$ chown -R mysql.mysql /var/lib/mysql/
```
b. 方法二:使用innobackupex命令进行恢复(推荐)
```shell
$ innobackupex --copy-back /backup/full/
$ chown -R mysql.mysql /var/lib/mysql/
```
7.2 恢复到pxc集群
恢复到pxc集群需要先停止所有pxc数据库节点，然后在单节点执行7.1内容后，还需要执行pxc模式的数据库的恢复操作
，恢复参考这个wiki：http://10.226.45.40:8090/document/index?document_id=596

